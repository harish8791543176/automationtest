package com.intranet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ImportResource("classpath:application-context.xml")
@PropertySource(value="classpath:intranet.properties")
public class IntranetInitializer  {
    public static void main(String[] args) {
        SpringApplication.run(IntranetInitializer.class, args);
    }
}