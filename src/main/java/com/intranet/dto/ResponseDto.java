package com.intranet.dto;

public class ResponseDto {

	private String message;
	private int Status;

	public String getMessage() {

		return message;
	}

	public int getStatus() {

		return Status;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public void setStatus(final int status) {
		Status = status;
	}

}
