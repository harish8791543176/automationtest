package com.intranet.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "employee")
public class EmployeeDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@PreUpdate
	public void preUpdate() {
		lastModified = DateTime.now();
	}

	@PrePersist
	public void prePersist() {
		DateTime current = DateTime.now();
		lastModified = current;
		createdAt = current;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "MOBILE_NO")
	private String mobileNo;

	@Column(name = "EMP_ID")
	private String employeeId;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "DESIGNATION")
	private String designation;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "YEAR_OF_JOINING")
	private DateTime yearOfJoining;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "CREATED_AT")
	private DateTime createdAt;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "LAST_MODIFIED")
	private DateTime lastModified;

	@Column(name = "PROFILE_IMAGE")
	private byte[] displayPicture;

	public DateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(DateTime lastModified) {
		this.lastModified = lastModified;
	}

	public Long getId() {
		return id;
	}

	public DateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public byte[] getDisplayPicture() {
		return displayPicture;
	}

	public void setDisplayPicture(byte[] displayPicture) {
		this.displayPicture = displayPicture;
	}

	public DateTime getYearOfJoining() {
		return yearOfJoining;
	}

	public void setYearOfJoining(DateTime yearOfJoining) {
		this.yearOfJoining = yearOfJoining;
	}

}