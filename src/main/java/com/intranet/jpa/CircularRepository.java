package com.intranet.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.intranet.entity.Circular;

public interface CircularRepository extends JpaRepository<Circular, Long> {

	Circular findByName(String name);

}
