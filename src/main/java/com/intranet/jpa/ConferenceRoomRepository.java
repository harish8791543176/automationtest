package com.intranet.jpa;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.intranet.entity.ConferenceRoomBooking;

@Repository
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoomBooking, Long> {
	ConferenceRoomBooking findByDateOfBookingAndBookingTimeAndTimeOut(String dateOfBooking, String bookingTime,
			String timeOut);

	ConferenceRoomBooking findByDateOfBookingAndBookingTimeAndTimeOutAndPassword(String dateOfBooking,
			String bookingTime, String timeOut, String password);
	
	ConferenceRoomBooking findByDateOfBookingAndBookingTimeGreaterThanEqualAndTimeOutLessThanEqual(String dateOfBooking, Date timein,
			Date timeout); 
}
