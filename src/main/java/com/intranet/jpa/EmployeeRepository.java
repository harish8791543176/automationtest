package com.intranet.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.intranet.entity.EmployeeDetail;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeDetail, Long> {

	EmployeeDetail findByEmployeeId(String employeeId);

}