package com.intranet.rest;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.Circular;
import com.intranet.service.CircularService;

@RequestMapping("/intranet")
@RestController
public class CircularController {

	@Inject
	private CircularService circularService;

	/**
	 * Method for delete a circular
	 *
	 * @param request
	 * @param response
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/deleteCircular", method = RequestMethod.DELETE)
	public ResponseDto deleteCircular(final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "name", required = true) final String name) {

		return circularService.deleteEvents(name);
	}

	/**
	 * Method for add Circular
	 *
	 * @param circularRequest
	 * @throws IOException
	 */
	@RequestMapping(value = "/addCircular", method = RequestMethod.POST)
	public ResponseDto saveCircular(final MultipartHttpServletRequest request) throws IOException {

		return circularService.addCircular(request);
	}

	/**
	 * Method for getting the list of circular
	 *
	 * @return
	 */
	@RequestMapping(value = "/viewCircular", method = RequestMethod.GET)
	public List<Circular> viewCircular() {

		return circularService.getAllCirculars();
	}
}
