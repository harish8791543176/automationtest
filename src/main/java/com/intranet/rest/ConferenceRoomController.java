package com.intranet.rest;

import java.text.ParseException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.ConferenceRoomBooking;
import com.intranet.service.ConferenceRoomService;

@RequestMapping("/intranet")
@RestController
public class ConferenceRoomController {

	@Inject
	private ConferenceRoomService conferenceRoomSerice;

	/**
	 * Method for Book a Conference Room
	 *
	 * @param request
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/bookConferenceRoom", method = RequestMethod.POST)
	public ResponseDto bookRoom(final HttpServletRequest request) throws ParseException {

		return conferenceRoomSerice.bookingRoom(request);
	}

	/**
	 * @param request
	 * @param response
	 * @param dateOfBooking
	 * @param bookingTime
	 * @param timeOut
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/cancelRoom", method = RequestMethod.DELETE)
	public ResponseDto cancelRoom(final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "date", required = true) final String dateOfBooking,
			@RequestParam(value = "bookingTime", required = true) final String bookingTime,
			@RequestParam(value = "timeOut", required = true) final String timeOut,
			@RequestParam(value = "password", required = true) final String password) {

		return conferenceRoomSerice.deleteRoom(dateOfBooking, bookingTime, timeOut, password);
	}

	/**
	 * Method for view Booked Conference Room
	 *
	 * @return
	 */
	@RequestMapping(value = "/conferenceRoom", method = RequestMethod.GET)
	public List<ConferenceRoomBooking> viewRoom() {

		return conferenceRoomSerice.conferenceRoomDetails();
	}
}