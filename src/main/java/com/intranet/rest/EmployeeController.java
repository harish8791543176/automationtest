package com.intranet.rest;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.EmployeeDetail;
import com.intranet.service.EmployeeService;

@RequestMapping("/intranet")
@RestController
public class EmployeeController {

	@Inject
	private EmployeeService employeeService;

	/**
	 * Method for delete a employee
	 *
	 * @param request
	 * @param response
	 * @param employeeId
	 */
	@RequestMapping(value = "/deleteEmployee", method = RequestMethod.DELETE)
	public ResponseDto deleteEmp(final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "employeeId", required = true) final String employeeId) {
		return employeeService.deleteEmployee(employeeId);
	}

	/**
	 * Method for Add Employee Detail
	 *
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
	public  ResponseDto saveUser(final MultipartHttpServletRequest request) throws IOException {

		return employeeService.saveEmployee(request);
	}

	/**
	 * Method for update a employee
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateEmployee", method = RequestMethod.POST)
	public  ResponseDto updateEmployee(final MultipartHttpServletRequest request) throws IOException {

		return employeeService.updateEmployee(request);
	}

	/**
	 * Method for getting the list of employee
	 *
	 * @return
	 */
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public List<EmployeeDetail> viewUser() {

		return employeeService.viewEmployee();
	}
}
