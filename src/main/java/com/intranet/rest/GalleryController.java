/**
 *
 */
package com.intranet.rest;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.Gallery;
import com.intranet.service.GalleryService;

@RequestMapping("/intranet")
@RestController
public class GalleryController {
	
	@Inject
	private GalleryService galleryService;

	ResponseDto responseDto = new ResponseDto();

	/**
	 * @param file
	 * @param session
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/addGallery", method = RequestMethod.POST)
	public ResponseDto saveGallery(@RequestParam final MultipartFile file, final HttpSession session)
			throws IOException {

		return galleryService.saveGalleryPath(file, session);
	}

	/**
	 * @return
	 */
	@RequestMapping(value = "/viewGallery", method = RequestMethod.GET)
	public List<Gallery> viewGallery() {

		return galleryService.viewGallery();
	}
}
