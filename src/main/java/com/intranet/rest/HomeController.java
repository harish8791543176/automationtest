/**
 *
 */
package com.intranet.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HomeController {

	@RequestMapping(value = { "/intranet", "/intranet_admin" }, method = RequestMethod.GET)
	ModelAndView loadadmin(final ModelMap model) {

		return new ModelAndView("admin.html", "admin.html", model);
	}

	@RequestMapping(value = { "/intranet" }, method = RequestMethod.GET)
	ModelAndView loadHomePage(final ModelMap model) {

		return new ModelAndView("index", "index", model);
	}
}
