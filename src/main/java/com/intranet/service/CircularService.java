package com.intranet.service;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.Circular;
import com.intranet.jpa.CircularRepository;

@Service
public class CircularService {

	@Inject
	private CircularRepository circularRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(CircularService.class.getName());
	ResponseDto responseDto = new ResponseDto();

	/**
	 * Method to add circular
	 *
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public ResponseDto addCircular(final MultipartHttpServletRequest request) throws IOException {
		Circular circular1 = new Circular();
		circular1 = circularRepository.findByName(request.getParameter("name"));
		if (isNull(circular1)) {
			LOGGER.info("Start Adding Circullar");
			final Circular circular = new Circular();
			circular.setName(request.getParameter("name"));
			request.getParameter("name");
			final List<MultipartFile> files = request.getFiles("file");
			for (final MultipartFile multiFile : files) {
				circular.setPdfFile(multiFile.getBytes());
			}
			circularRepository.save(circular);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setMessage(" Circular added succesfully");
			LOGGER.info("[In CircularService  >> addCircular ] , Circular Added successfully");
		} else {
			LOGGER.info("[In CircularService  >> addCircular ] , Circular Already Added");
			responseDto.setMessage("Already Exist");
		}
		return responseDto;

	}

	/**
	 * Method to delete a Circular
	 *
	 * @param namenameOfCircular
	 * @return
	 */
	public ResponseDto deleteEvents(final String nameOfCircular) {
		final Circular circular = circularRepository.findByName(nameOfCircular);
		if (nonNull(circular)) {
			LOGGER.info("[In CircularService  >> deleteEvents ] , Deleting circullar");
			circularRepository.delete(circular);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setMessage("Circular deleted successfully");
			LOGGER.info("[In CircularService  >> deleteEvents ] , Deleted circullar");
		} else {
			responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
			responseDto.setMessage("error");
			LOGGER.warn(" [In CircularService  >> deleteEvents ] , Can't be deleted ");
		}

		return responseDto;
	}

	/**
	 * Method to get list of circular
	 *
	 * @return
	 */
	public List<Circular> getAllCirculars() {
		LOGGER.info("[CircularService >> getAllCirculars] , loading circullar");

		return circularRepository.findAll();
	}

}
