package com.intranet.service;

import static java.util.Objects.nonNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.ConferenceRoomBooking;
import com.intranet.entity.EmployeeDetail;
import com.intranet.jpa.ConferenceRoomRepository;
import com.intranet.jpa.EmployeeRepository;
import com.intranet.utilities.SessionIdentifierGenerator;

@Service
public class ConferenceRoomService {

	@Inject
	private ConferenceRoomRepository conferenceRoomRepository;

	@Inject
	private EmployeeRepository employeeRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(ConferenceRoomService.class.getName());
	private ConferenceRoomBooking conferenceRoom;
	SessionIdentifierGenerator randomPassword = new SessionIdentifierGenerator();
	ResponseDto responseDto = new ResponseDto();

	/**
	 * Method to book conference room
	 *
	 * @param request
	 * @return
	 * @throws ParseException 
	 */
	public ResponseDto bookingRoom(final HttpServletRequest request) throws ParseException {
		DateFormat sdf = new SimpleDateFormat("HH:mm");
		Date timein = sdf.parse(request.getParameter("timein"));
		Date timeout = sdf.parse(request.getParameter("timeout"));
		System.out.println("Time: " + sdf.format(timein));
		final ConferenceRoomBooking checkRoom = conferenceRoomRepository.findByDateOfBookingAndBookingTimeGreaterThanEqualAndTimeOutLessThanEqual(
				request.getParameter("date"), timein, timeout);

		if (nonNull(checkRoom)) {
			responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
			responseDto.setMessage("room already booked");
		} else {
			LOGGER.info("[ In ConferenceRoomService >> bookingRoom ] , Start Booking room");
			final ConferenceRoomBooking conferenceRoom = new ConferenceRoomBooking();
			conferenceRoom.setEmployeeId(request.getParameter("empId"));
			conferenceRoom.setStatus("Active");
			conferenceRoom.setDateOfBooking(request.getParameter("date"));
			conferenceRoom.setBookingTime(request.getParameter("timein"));
			conferenceRoom.setTimeOut(request.getParameter("timeout"));
			final String nn = randomPassword.nextSessionId();
			conferenceRoom.setPassword(nn);
			final EmployeeDetail employeeDetail = employeeRepository.findByEmployeeId(request.getParameter("empId"));
			if (nonNull(employeeDetail)) {
				conferenceRoom.setEmployeeName(employeeDetail.getFirstName());
			}

			conferenceRoomRepository.save(conferenceRoom);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setMessage("Room Booked Succesfully");
			LOGGER.info("[ In ConferenceRoomService >> bookingRoom ] , booked succesfully");
		}

		return responseDto;
	}

	/**
	 * Method to canceled booked room
	 *
	 * @param bookingTime
	 * @param timeOut
	 * @return
	 */
	public ResponseDto deleteRoom(final String dateOfBooking, final String bookingTime, final String timeOut,
			final String password) {
		conferenceRoom = conferenceRoomRepository.findByDateOfBookingAndBookingTimeAndTimeOutAndPassword(dateOfBooking,
				bookingTime, timeOut, password);
		if (nonNull(conferenceRoom)) {
			LOGGER.info("[ In ConferenceRoomService >> deleteRoom ] , Deleting Booking room");
			conferenceRoomRepository.delete(conferenceRoom);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setMessage("Room canceled Succesfully");
		} else {
			responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
			responseDto.setMessage("Can't cancel This Room");
			LOGGER.warn("[ In ConferenceRoomService >> deleteRoom ] , Can't cancel This Room");
		}

		return responseDto;
	}

	/**
	 * Method to get list of booked history
	 *
	 * @return
	 */
	public List<ConferenceRoomBooking> conferenceRoomDetails() {
		LOGGER.info("[ In ConferenceRoomService >> conferenceRoomDetails ] , Getting Detail Booking room");

		return conferenceRoomRepository.findAll();
	}
}