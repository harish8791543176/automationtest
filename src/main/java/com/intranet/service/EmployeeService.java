package com.intranet.service;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.EmployeeDetail;
import com.intranet.jpa.EmployeeRepository;

@Service
public class EmployeeService {

	@Inject
	private EmployeeRepository employeeRepo;

	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("YYYY-MM-DD");
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeService.class.getName());
	ResponseDto responseDto = new ResponseDto();

	/**
	 * Method to delete a employee
	 *
	 * @param employeeId
	 * @return
	 */
	public ResponseDto deleteEmployee(final String employeeId) {
		final EmployeeDetail employeeDetail = employeeRepo.findByEmployeeId(employeeId);
		if (nonNull(employeeDetail)) {
			employeeRepo.delete(employeeDetail);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setMessage("deleted successfully");
			LOGGER.info("[In EmployeeService >> deleteEmployee] , Employee deleted Successfully");
		} else {
			responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
			responseDto.setMessage("user doesn't exist");
			LOGGER.warn("[In EmployeeService >> deleteEmployee] , deleting employee process not compleed");
		}

		return responseDto;
	}

	/**
	 * Method for save employee data
	 *
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public ResponseDto saveEmployee(final MultipartHttpServletRequest request) throws IOException {
		final EmployeeDetail checkEmployee = employeeRepo.findByEmployeeId(request.getParameter("employeeId"));
		if (isNull(checkEmployee)) {
			LOGGER.info("Adding employee");
			final EmployeeDetail employeeDetail = new EmployeeDetail();
			employeeDetail.setFirstName(request.getParameter("firstName"));
			employeeDetail.setLastName(request.getParameter("lastName"));
			employeeDetail.setEmail(request.getParameter("email"));
			employeeDetail.setMobileNo(request.getParameter("mobileNo"));
			employeeDetail.setEmployeeId(request.getParameter("employeeId"));
			employeeDetail.setStatus(request.getParameter("status"));
			employeeDetail.setYearOfJoining(DATE_FORMAT.parseDateTime(request.getParameter("yearOfJoining")));
			employeeDetail.setDesignation(request.getParameter("designation"));

			final List<MultipartFile> files = request.getFiles("myfile");
			for (final MultipartFile multiFile : files) {
				employeeDetail.setDisplayPicture(multiFile.getBytes());
			}
			employeeRepo.save(employeeDetail);
			responseDto.setStatus(200);
			responseDto.setMessage("Employee added succesfully");
			LOGGER.info("[In EmployeeService >> saveEmployee] , Employee Detail Added");
		} else {
			LOGGER.warn("[In EmployeeService >> saveEmployee] ,Employee Detail Already added");
			responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
			responseDto.setMessage("user already exist");
		}

		return responseDto;
	}

	/**
	 * Method to update a employee
	 *
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public ResponseDto updateEmployee(final MultipartHttpServletRequest request) throws IOException {
		final EmployeeDetail employeeDetail = employeeRepo.findByEmployeeId(request.getParameter("employeeId"));
		if (nonNull(employeeDetail)) {
			employeeDetail.setEmployeeId(request.getParameter("employeeId"));
			employeeDetail.setFirstName(request.getParameter("firstName"));
			employeeDetail.setLastName(request.getParameter("lastName"));
			employeeDetail.setEmail(request.getParameter("email"));
			employeeDetail.setMobileNo(request.getParameter("mobileNo"));
			employeeDetail.setStatus(request.getParameter("status"));
			employeeDetail.setDesignation(request.getParameter("designation"));
			final List<MultipartFile> files = request.getFiles("myfile");
			for (final MultipartFile multiFile : files) {
				employeeDetail.setDisplayPicture(multiFile.getBytes());
			}
			employeeRepo.save(employeeDetail);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setMessage("updatate succesfully");
			LOGGER.info("[EmplyeeService >> updateEmployee] , Employee updated successfully");
		} else {
			responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
			responseDto.setMessage("failed");
			LOGGER.warn("[In EmployeeService >> updateEmployee] ,Failed to update employee");
		}

		return responseDto;
	}

	/**
	 * Method to getting list of emplyee
	 *
	 * @return
	 */
	public List<EmployeeDetail> viewEmployee() {
		LOGGER.info("[In EmployeeService >> updateEmployee] , Loading employee Detail");

		return employeeRepo.findAll();
	}
}
