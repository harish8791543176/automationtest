/**
 *
 */
package com.intranet.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.intranet.dto.ResponseDto;
import com.intranet.entity.Gallery;
import com.intranet.jpa.GalleryRepository;

@Service
public class GalleryService {

	@Inject
	private GalleryRepository galleryRepository;

	private static final String UPLOAD_DIRECTORY = "/uploadImages";
	private static final Logger LOGGER = LoggerFactory.getLogger(GalleryService.class.getName());
	ResponseDto responseDto = new ResponseDto();

	/**
	 * Method to add image i src and url in DB
	 * 
	 * @param request
	 * @param session
	 * @return
	 * @throws IOException
	 */
	public ResponseDto saveGalleryPath(final MultipartFile request, final HttpSession session) throws IOException {
		final Gallery gallery1 = galleryRepository.findByName(request.getOriginalFilename());
		if (gallery1 == null) {
			LOGGER.info("[ In GalleryService >> saveGalleryPath] ,Start Adding Image");
			final Gallery gallery = new Gallery();
			final ServletContext context = session.getServletContext();
			final String path = context.getRealPath(UPLOAD_DIRECTORY);
			final String filename = request.getOriginalFilename();
			final String url = "uploadImages/" + filename;
			final byte[] bytes = request.getBytes();
			final BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(new File(path + File.separator + filename)));
			stream.write(bytes);
			stream.flush();
			stream.close();
			gallery.setName(request.getOriginalFilename());
			gallery.setImageUrl(url);
			gallery.setSize(request.getSize());
			galleryRepository.save(gallery);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setMessage("image added succesfully");
			LOGGER.info("[ In GalleryService >> saveGalleryPath] , Added Image successfully");
		} else {
			responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
			responseDto.setMessage("image already exist");
		}

		return responseDto;
	}

	/**
	 * Method to get List OF Image
	 * @return
	 */
	public List<Gallery> viewGallery() {
		// TODO Auto-generated method stub
		LOGGER.info("[In GalleryService >> viewGallery ] , Loading Gallery");

		return galleryRepository.findAll();
	}
}
