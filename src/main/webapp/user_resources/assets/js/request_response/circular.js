/**
 * @returns
 */
function viewCircular() {
	jQuery.ajax({
		type : "GET",
		url : "intranet/viewCircular",
		success : function(response) {
			data = response;
			handleCircularResponse(data);
		},
		error : function(e) {
			alert("error while trying to retrive data")
		}
	});
}

function handleCircularResponse(data) {
	var noOfCircular = data.length;
	var index = 1;
	for (var i = 0; i < noOfCircular; i++) {
		var name = data[i].name;
		var createdAt = data[i].createdAt;
		var myfile = data[i].pdfFile;
		var file = "data:application/pdf;base64," + myfile;
		var adding = "<tr style='color:#000;'><td>"
			+ index
			+ "</td><td>"
			+ name
			+ "</td><td> "
			+ createdAt.dayOfMonth
			+ "/"
			+ createdAt.dayOfWeek
			+ "/"
			+ createdAt.year
			+ "</td><td><a href = "
			+ file
			+ " target='_blank'><img src='user_resources/assets/images/pdf-icon.png' alt='pdf'></a></td></tr>";
		jQuery("#circular tbody").append(adding);
		index++;

	}
}
