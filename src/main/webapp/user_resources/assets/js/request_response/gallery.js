function showGallery() {
	//alert("alert");
	jQuery.ajax({
		type : "GET",
		url : "intranet/viewGallery",
		success : function(response) {
			data = response;
			handleGalleryResponse(data);
		},
		error : function(e) {
			alert("error while trying to retrive data");
		}
	});

}

function handleGalleryResponse(data) {
	var noOfimage = data.length;
	for (var i = 0; i < noOfimage; i++) {
		var imageUrl = data[i].imageUrl;
		var name = data[i].name;
		/*jQuery("#mygallery").append(jQuery('<div id="gallery_image"><img src='+imageUrl+' class="img-rounded" alt="image not available"></div>'));*/
		jQuery("#mygallery")
		.append(
				jQuery('<div class="col-md-4"><div class="thumbnail" id="gallery_image"><a href='
						+ imageUrl
						+ 'target="_blank"><img src='
						+ imageUrl
						+ ' alt="Lights" style="width:100%"><div class="caption text-center"><p>'
						+ name + '</p></div></a></div></div>'));

	}

}