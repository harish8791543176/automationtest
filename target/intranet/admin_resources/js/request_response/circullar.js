jQuery("#addcircle").click(function() {
	var formvalue = new FormData($('#addCircullar')[0]);

    //serialize data function

    $.ajax({
        type: "POST",
        url: "intranet/addCircular",
        data: formvalue ,
        processData: false,
        contentType: false,
        success: function(response) {
        	  $('#your-modal').modal('toggle');
     	     $("#header_success").empty();
     	     $("#header_success").append("Response");
     	     $("#message_success").empty();
     	     $("#message_success").append(response.message);
     	     $("#addCircullar").trigger("reset");
     	    
        },
        error: function(e) {
        	  $('#your-modal').modal('toggle');
     	     $("#header_success").empty();
     	     $("#header_success").append("Response");
     	     $("#message_success").empty();
     	     $("#message_success").append(e.message);
        }

    });

});

function viewCircular() {
    jQuery.ajax({
        type: "GET",
        url: "intranet/viewCircular",
        success: function(response) {
            data = response;
            handleCircularResponse(data);
        },
        error: function(e) {
            alert("error while trying to retrive data")
        }
    });
}

 function handleCircularResponse(data) {
    var noOfCircullar = data.length;
    var index=1;
    for (var i = 0; i < noOfCircullar; i++) {
        var name = data[i].name;
        var createdAt = data[i].createdAt;
        var file = data[i].file;

        var adding = "<tr id="+ index +"><td>" +
        index+"</td><td>"+
            name +
           "</td><td> " +
            createdAt.dayOfMonth + 
            "/" + createdAt.dayOfWeek +
            "/" + createdAt.year +
            "</td><td><button type='submit' class='btn btn-danger'  data-toggle='modal' data-target='#deleteCircle' onclick='selectCircle("+index +
            ")'>Delete</button></td></tr>";
        jQuery("#circular tbody").append(adding);
        index++;
       
    }
 }

 function selectCircle(trId) {
	 var id1 = trId;
	 var tr = document.getElementById(id1);
	 var td = tr.getElementsByTagName("td");
	 var tdLength = td.length;
	 //alert(tr.innerHTML);
	 $("#nameOfEvent").val($.trim(td[1].innerHTML));
	 //alert(td[1].innerHTML);
 }
	  function deleteCircle() {
	 var name = $("#nameOfEvent").val();

	 $.ajax({
	  type: "DELETE",
	  contentType: false,
	  url: "intranet/deleteCircular?name=" + name + "",

	  success: function(response) {
		  location.reload();
	   $('#your-modal').modal('toggle');
	     $("#header_success").empty();
	     $("#header_success").append("Response");
	     $("#message_success").empty();
	     $("#message_success").append(response.message);
	  },
	  error: function(e) {
		  $('#your-modal').modal('toggle');
		     $("#header_success").empty();
		     $("#header_success").append("Response");
		     $("#message_success").empty();
		     $("#message_success").append(e.message);
	  }

	 });
	}