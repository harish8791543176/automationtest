// To add Employee Detaill
function employeeDetail() {

 $("#submit_emp").click(function(event) {

  var formValid = employeeDetail.formValidation();
  if (formValid) {
   var formvalue = new FormData($('#addForm')[0]);
   $.ajax({
    url: "intranet/addEmployee",
    type: "POST",
    data: formvalue,
    contentType: false,
    processData: false,
    success: function(response) {
     $('#your-modal').modal('toggle');
     $("#header_success").empty();
     $("#header_success").append("Response");
     $("#message_success").empty();
     $("#message_success").append(response.message);
     $('#addForm').trigger("reset");
    },
    error: function(e) {
     $('#your-modal').modal('toggle');
     
     $("#message_success").empty();
     $("#message_success").append(e.message);
    }
   });
  } else {
   return false;
  }
 });

 this.formValidation = function() {
  var isValid = true;
  
  if (!($("#emp_id").val())) {
   $("#valid_msg_employee_id").html("Warning Please fill EmployeeId");
   $("#emp_id").focus();
   isValid = false;
  } else {
   $("#valid_msg_employee_id").hide();
  }
  if (!($("#firstName").val())) {
   $("#valid_msg_fname").html("First Name cannot be empty");
   $("#valid_msg_fname").show();
   $("#firstName").focus();
   isValid = false;
  } else {
   var letters = /^[A-Za-z]+$/;
   var empname = $("#firstName").val();
   if (empname.match(letters)) {

    if (empname.length < 1) {

     $("#valid_msg_fname").html("<p>First name must be conatin 2 character <p>");
     $("#valid_msg_fname").show();
     $("#firstName").focus();
     isValid = false;
    } else {
     $("#valid_msg_fname").hide();
    }
   } else {
    $("#valid_msg_fname").html("Enter valid name");
    $("#valid_msg_fname").show();
    isValid = false;
   }
  }

  if (!($("#lastName").val())) {
   $("#valid_msg_lname").html("lastname cannot be empty");
   $("#valid_msg_lname").show();
   $("#lastName").focus();
   isValid = false;
  } else {
   var letters = /^[A-Za-z]+$/;
   var empname = $("#firstName").val();
   if (empname.match(letters)) {

    if (empname.length < 1) {

     $("#valid_msg_lname").html("<p>username must be conatin 2 character <p>");
     $("#valid_msg_lname").show();
     $("#lastName").focus();
     isValid = false;
    } else {
     $("#valid_msg_lname").hide();
    }
   } else {
    $("#valid_msg_lname").html("Enter valid last name");
    $("#valid_msg_lname").show();
    isValid = false;
   }
  }

  var designation = $("#dropdown").val();
  if (designation == "Select") {
   $("#valid_msg_designation").html("Please Select Designation");
   $("#valid_msg_designation").show();
   $("#designation").focus();
   isValid = false;
  } else {
   $("#valid_msg_designation").hide();
  }

  if (!($("#doj").val())) {
   $("#valid_msg_year").html("Warning Please Enter Year of joining");
   $("#doj").focus();
   isValid = false;
  } else {
   var joiningyear = $("#doj").val();
   if (joiningyear.length > 0) {
    $("#valid_msg_year").hide();

   } else {
    $("#valid_msg_year").html("Warning Please Enter Year of joining");
    $("#valid_msg_year").show();

    isValid = false;
   }
  }

  if (!($("#email").val())) {
   $("#valid_msg_email").html("Warning Please fill Valid E-mail");
   $("#email").focus();
   isValid = false;
  } else {
   var mail = $("#email").val();
   var validate_char = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
   if (mail.match(validate_char)) {
    $("#valid_msg_email").hide();
   } else {

    $("#valid_msg_email").html("Warning Please fill Valid E-mail");
    $("#valid_msg_email").show();
    $("#email").focus();
    isValid = false;
   }
  }
  if (!($("#contact").val())) {
   $("#valid_msg_contact").html("Warning Please fill Valid Contact");
   $("#contact").val(NA);
   $("#valid_msg_contact").show();
   isValid = false;
  } else {
   var contact = $("#contact").val();
   var contactExp = /^\d{10}$/;
   if (contact.match(contactExp)) {
    $("#valid_msg_contact").hide();

   } else {
    $("#valid_msg_contact").html("Warning Please fill Valid Contact");
    $("#valid_msg_contact").show();
    isValid = false;
   }
  }

  return isValid;

 }


}

//Show employee Detail
function showEmployee() {
 //alert("alert");
 jQuery.ajax({
  type: "GET",
  url: "intranet/employees",
  success: function(response) {
   data = response;
   handleEmployeeResponse(data);
  },
  error: function(e) {
   alert("error while trying to retrive data")
  }
 });

}

function handleEmployeeResponse(data) {
 var noOfEmployee = data.length;
 var index = 1;
 for (var i = 0; i < noOfEmployee; i++) {
  var employeeId = data[i].employeeId;
  var firstName = data[i].firstName;
  var lastName = data[i].lastName;
  var designation = data[i].designation;
  var yearOfJoining = data[i].yearOfJoining;
  var email = data[i].email;
  var contact = data[i].mobileNo;
  var status = data[i].status;
  var image = data[i].image;


  // document.getElementById("ItemPreview").src = "data:image/png;base64," +image;

  var adding = "<tr id=" + i + "><td>" +
   index + "</td><td>" +
   employeeId + "</td><td> " +
   firstName + "</td><td>" +
   lastName + "</td><td> " +
   designation + "</td><td> "
	+ yearOfJoining.dayOfMonth + '/' + yearOfJoining.dayOfWeek + '/'
	+ yearOfJoining.year + 
    "</td><td> " +
   email + "</td><td>" +
   contact + "</td><td>" + status + "</td><td><button  class='btn btn-danger'  data-toggle='modal' data-target='#editEmployee' onclick='selectEmployee(" + i + ");'>Edit</button></td></tr>";
  jQuery("#dataTable tbody").append(adding);
  index++;
 }
}

//Update employee Detail
function updateEmployeeDetail() {

 //serialize data 
 var formvalue = new FormData($('#updateForm')[0]);
 $.ajax({
	 url: "intranet/updateEmployee",
	    type: "POST",
	    data: formvalue,
	    contentType: false,
	    processData: false,
  /*dataType : 'json',*/
  success: function(response) {
	
	  $('#your-modal').modal('toggle');
	   $("#header_success").empty();
	   $("#header_success").append("Response");
	   $("#message_success").empty();
	   $("#message_success").append(response.message);
  },
  error: function(e) {
	  $('#your-modal').modal('toggle');
	     $("#header_success").empty();
	     $("#header_success").append("Response");
	     $("#message_success").empty();
	     $("#message_success").append(e.message);
  }

 });

 function printResponse() {
  if (data == 200) {
   alert("update sucessfully")
  } else if (data == 400) {
   alert("failed ")
  }
 }
}

function doSearch() {
 var input = document.getElementById('searchTerm').value;
 var searchText = input.toUpperCase();
 var targetTable = document.getElementById('dataTable');
 var targetTableColCount;

 //Loop through table rows
 for (var rowIndex = 0; rowIndex < targetTable.rows.length; rowIndex++) {
  var rowData = '';

  //Get column count from header row
  if (rowIndex == 0) {
   targetTableColCount = targetTable.rows.item(rowIndex).cells.length;
   continue; //do not execute further code for header row.
  }

  //Process data rows. (rowIndex >= 1)
  for (var colIndex = 0; colIndex < targetTableColCount; colIndex++) {
   rowData += targetTable.rows.item(rowIndex).cells.item(colIndex).textContent.toUpperCase();
  }

  //If search term is not found in row data
  //then hide the row, else show
  if (rowData.indexOf(searchText) == -1)
   targetTable.rows.item(rowIndex).style.display = 'none';
  else
   targetTable.rows.item(rowIndex).style.display = 'table-row';
 }
}

//update a row insert value into field of modal
function selectEmployee(trId) {
 var id1 = trId;
 var tr = document.getElementById(id1);
 var td = tr.getElementsByTagName("td");
 var tdLength = td.length;
 $("#emp_id").val(td[1].innerHTML);
 $("#firstName").val($.trim(td[2].innerHTML));
 $("#lastName").val(td[3].innerHTML);
 $("#dropdown").val($.trim(td[4].innerHTML));
 $("#doj").val($.trim(td[5].innerHTML));
 $("#status").val(td[8].innerHTML);
 $("#email").val(td[6].innerHTML);
 $("#contact").val(td[7].innerHTML);
 //console.log(td[0].innerHTML);

}

function deleteEmployee() {
 var employeeId = $("#emp_id").val();

 $.ajax({
  type: "DELETE",
  contentType: "application/json; charset=utf-8",
  url: "intranet/deleteEmployee?employeeId="+employeeId+"",

  success: function(response) {
	  
   $('#your-modal').modal('toggle');
   $("#header_success").empty();
   $("#header_success").append("Response");
   $("#message_success").empty();
   $("#message_success").append(response.message);

  },
  error: function(e) {
	  $('#your-modal').modal('toggle');
	     $("#header_success").empty();
	     $("#header_success").append("Response");
	     $("#message_success").empty();
	     $("#message_success").append(e.message);
  }

 });
}
var employeeDetail = new employeeDetail();