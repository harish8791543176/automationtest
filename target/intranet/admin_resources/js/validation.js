$(document).ready(function()
{
      function formValidation() { 
    	  var isValid = true;
       
       $("#emp_id").focusout(function()
       {
    	  check_empid();
       });
       
       $("#firstName").focusout(function()
       {
    	   check_fname();
       });
       
       $("#lastName").focusout(function()
       {
    	   check_lname();
       });
       
       $("#designation").focusout(function()
       {
    	   check_designation();
       });
       
       $("#doj").focusout(function()
       {
    	   check_joiningYear();
       });
       
       $("#userDesignation").focusout(function()
       {
              check_designation();
       });
       
       $("#email").focusout(function()
    	       {
    	   check_email();
    	       });
       
       $("#contact").focusout(function()
    	       {
    	   check_contact();
    	       });
		
      function  check_empid() {
			
    	  if (!($("#emp_id").val())) {
  			$("#valid_msg_employee_id").html("Warning Please fill EmployeeId");
  			$("#emp_id").focus();
  			isValid = false;
  			} else{
   				$("#valid_msg_employee_id").hide();
   			}
      }	
    	   
       function check_fname() {
    	 
           var empname_length=$("#firstName").val();
           var letters =  /^[A-Za-z]+$/;
           if(empname_length.length==0) {
           $("#valid_msg_fname").html("Username cannot be empty");
           $("#valid_msg_fname").show();
           $("#firstName").focus(); 
           isValid = false;
           }
           else if(empname_length.match(letters)) {
           if (empname_length.length<1) {
                  
                  $("#valid_msg_fname").html("<p>username must be conatin 2 character <p>");
                  $("#valid_msg_fname").show();
                  isValid = false;
           }
            else{
                  $("#valid_msg_fname").hide();
                 
           }
           }
           else {
                  $("#valid_msg_fname").html("Enter valid name");
                  $("#valid_msg_fname").show();
                  isValid = false;
           }
    }
 
       /* Validation for Last Name */
       
       function check_lname() {
    	  
           var empname_length=$("#lastName").val();
           var letters =  /^[A-Za-z]+$/;
        
           if(empname_length.match(letters))
	          {
	           if (empname_length.length<1){
	                  
	                  $("#valid_msg_lname").html("<p>username must be conatin two character <p>");
	                  $("#valid_msg_lname").show();
	                  $("#lastName").focus(); 
	                  isValid = false;
	           }
	           else {
	                  $("#valid_msg_lname").hide();
	                  
	           }
	          }
           else {
                  $("#valid_msg_lname").html("Enter valid name");
                  $("#valid_msg_lname").show();
                  $("#lastName").focus(); 
                  isValid = false;
           }
    }
       
      
       /* Validation for Designation */
       
       function check_designation(){
    	   
           var designation_length=$("#designation").val();
     var letters =  /^[A-Za-z]+$/;
           if(designation_length=="Select") {
           $("#valid_msg_designation").html("Designation cannot be empty");
           $("#valid_msg_designation").show();
           $("#designation").focus(); 
           isValid = false;
           }
           else {
        	   $("#valid_msg_designation").hide();
           }
    }
      /* Validation function for year. */
       
   	function check_joiningYear() {
   		
   		var joiningyear=$("#doj").val();
   		/*var yearExp = /^\d{10}$/;*/
   		if(joiningyear.length == 0) {
	  $("#valid_msg_year").html("Joining date cannot be empty");
			$("#valid_msg_year").show();
			 $("#doj").focus(); 
			isValid = false;
   		}
   		else if(joiningyear.length > 0) {
   		 $("#valid_msg_year").hide();
   	 
   		}
   	}
   	
   	/* Validation of the Email */
   	 
    function check_email() {
  
        var mail=$("#email").val();
        var validate_char= /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if(mail.length==0)
        {
        $("#valid_msg_email").html("Email address cannot be empty");
        $("#valid_msg_email").show();
        $("#email").focus(); 
        isValid = false;
        }
        else if(mail.match(validate_char)) {
               
               $("#valid_msg_email").hide();
               return true;
        }
        else {
               
               $("#valid_msg_email").html("<p>Please enter valid email address <p>");
               $("#valid_msg_email").show();
               $("#email").focus(); 
               isValid = false;
        }
         }
    

    /* Validation function for contact */
	
    function check_contact() {
		var contact=$("#contact").val();
		var contactExp = /^\d{10}$/;
		if(contact.match(contactExp)) {
			$("#valid_msg_contact").hide();
			 
		}
		else if(contact.length==0) {
	   			$("#valid_msg_contact").html("Enter the valid contact");
	   			$("#valid_msg_contact").show();
	   			$("#contact").val(1234567890); 
	   			isValid = false;
		}
		else {
			 $("#valid_msg_contact").html("Enter the valid contact");
             $("#valid_msg_contact").show();
             $("#contact").focus(); 
             isValid = false;
		}
	}
      }
      formValidation();
	});
	

   	