var m1=angular.module('myApp',['ngRoute']);

// Routes configuration of the application
m1.config( function($routeProvider) {
    
	$routeProvider
			.when('/', {
	       templateUrl: 'home.html',
	       //controller: 'HomeController'
	         
	       })
   	   .when('/circular', {
       templateUrl: 'circle.html',
       controller: 'CircularController'
         
       })
       .when('/gallery', {
       templateUrl: 'gallery.html',
       controller: 'GalleryController'
        
       })
       .when('/employees', {
       templateUrl: 'employee.html',
       controller: 'EmployeeController'
        
       })
       .when('/login', {
       templateUrl: 'login.html',
       //controller: 'LoginController'
         
       })
	  .when('/conference', {
	   templateUrl: 'conference.html',
	   controller: 'ConferenceController'
	        
	   });
});

m1.controller('CircularController', function($rootScope, $http) {
	
	$rootScope.loadCircular=function(){
		//alert("loading circular");
    $http.get("intranet/viewCircular")
    .then(function(response) {
    	
    /*	var len=Object.keys(response.data).length;//length of data object is obtained
		//alert("received data: "+JSON.stringify(response.data));
		if(len>0)
			{
			if(angular.isArray(response.data.list)) {
				$rootScope.circularList=response.data.list;
			alert(circularList[0].name);}
			else
			{
				var ul=[];
				ul[0]=response.data.list;
				$rootScope.circularList=ul;
				alert(JSON.stringify(ul))
			}	
			}
		else
			{
			alert("No circular found.","alert-danger");
			
			}*/
    	//alert(JSON.stringify(response.data));
    	$rootScope.circularList=response.data;
       });
	}
});

m1.controller('EmployeeController', function($rootScope, $http){
	$rootScope.loadEmployee=function() {
		$http.get("intranet/employees")
		.then(function(response) {
			alert("received data: "+JSON.stringify(response.data));
			$rootScope.employees=response.data;
			var file =response.data.imgdata;
			alert(file);
		});
	}
});

m1.controller('GalleryController', function($scope, $http) {
	$scope.loadGallery = function() {
	$http.get("intranet/viewGallery")
	.then(function(response) {
		alert("received data: "+JSON.stringify(response.data));
	});
	}
	});
