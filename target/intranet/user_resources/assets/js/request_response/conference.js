/**
 * @returns
 */
function bookMyRoom() {
	var formvalue = new FormData($('#conference_form')[0]);

	jQuery.ajax({
		type : "POST",
		processData : false,
		contentType : false,
		url : "intranet/bookConferenceRoom",
		data : formvalue,
		success : function(response) {
			jQuery('#your-modal').modal('toggle');
			$("#header_success").empty();
			$("#header_success").append("Response");
			$("#message_success").empty();
			$("#message_success").append(response.message);
			// $('#conference_form').trigger("reset");
		},
		error : function(e) {
			jQuery('#your-modal').modal('toggle');
			$("#header_success").empty();
			$("#header_success").append("Response");
			$("#message_success").empty();
			$("#message_success").append(e.message);
		}
	});
}

function viewConference() {
	jQuery.ajax({
		type : "GET",
		url : "intranet/conferenceRoom",
		success : function(response) {
			data = response;
			handleConferenceRoomData(data);
		}

	});
}

function handleConferenceRoomData(data) {
	var dataLength = data.length;
	var index = 1;
	for (var i = 0; i < dataLength; i++) {
		var employeeId = data[i].employeeId;
		var name = data[i].employeeName;
		var date = data[i].dateOfBooking;
		var timeIn = data[i].bookingTime;
		var timeOut = data[i].timeOut;
		var status = data[i].status;

		var adding = "<tr id="
				+ i
				+ "><td>"
				+ index
				+ "</td><td>"
				+ employeeId
				+ "</td><td> "
				+ name
				+ "</td><td>"
				+ date
				+ "</td><td> "
				+ timeIn
				+ "</td><td> "
				+ timeOut
				+ "</td><td> "
				+ status
				+ "</td><td><button  class='btn btn-primary btn-xs'  data-toggle='modal' data-target='#deleteBookingRoom' onclick='deleteRoom("
				+ i + ");'>Delete</button></td></tr>";
		jQuery("#dataTable tbody").append(adding);
		index++
	}
}

function deleteRoom(trId) {
	var id1 = trId;
	var tr = document.getElementById(id1);
	var td = tr.getElementsByTagName("td");
	var tdLength = td.length;

	$("#date").val($.trim(td[3].innerHTML));
	$("#timein").val($.trim(td[4].innerHTML));
	$("#timeout").val($.trim(td[5].innerHTML));
}

function deleteConferenceRoom() {
	var date = $("#date").val();
	var timein = $("#timein").val();
	var timeout = $("#timeout").val();
	var password = $("#password").val();

	$.ajax({
		type : "DELETE",
		contentType : false,
		url : "intranet/cancelRoom?date=" + date + "&bookingTime=" + timein
				+ "&timeOut=" + timeout + "&password=" + password + "",
		success : function(response) {
			jQuery('#your-modal').modal('toggle');
			$("#header_success").empty();
			$("#header_success").append("Response");
			$("#message_success").empty();
			$("#message_success").append(response.message);
		},
		error : function(e) {
			jQuery('#your-modal').modal('toggle');
			$("#header_success").empty();
			$("#header_success").append("Response");
			$("#message_success").empty();
			$("#message_success").append(e.message);
		}

	});
}